Alkalmazás neve, Dressing by weather.


Az alkalmazás célja, hogy öltözéket ajánljon, a megadott város hőmérséklete alapján.
Az alkalmazás elindítása után megjelenő főképernyőn (fragment_home.xml) megjelenik az app neve, a logója, egy beviteli mező (editText) melyben beírhatjuk a keresett város nevét, illetve a GPS gombra kattintva a telefon helymeghatározását használva, meghatározza melyik városban tartózkodunk, és kitölti a beviteli mezőt.
Ezután a Keresés gombra kattintva átnavigál arra az oldalra (fragment_result.xml) ahol kiírja az adott város hőmérsékletét, és a hőmérséklettől függően eltérő ruhákat fog megjeleníteni. 

A hőmérsékleti sávok: 
-	kisebb egyenlő 5
-	nagyobb mint 5 és kisebb, egyenlő mint 10 
-	nagyobb mint 10 és kisebb egyenlő mint 21
-	nagyobb mint 21

Ha a beviteli mezőt nem töltjük ki, egy hibaüzenetet kapunk mely az „Adj meg egy várost!” hibaüzenetet jelenít meg. 
Ha hibás városnevet adunk meg, akkor a fragment_result.xml oldalon kapunk egy hibaüzenetet mely az „Ellenőrizd a megadott város nevét!” hibaüzenetet jelenít meg.

A Toolbar-ban található a Rólunk menü, tartalmazza az alkalmazás funkcióit, a készítők nevét, és Neptun kódját.
